"""Proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from tesis import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('',views.inicio, name='inicio'),
    path('tabla_estudiante/',views.tabla_estudiante, name='tabla_estudiante'),
    path('profesores/',views.profesores, name='tabla_estudiante'),
    path('academicos/', views.academicos, name='academicos'),
    path('tesis/', views.tesis, name='tesis'),
    path('tesis/agregar', views.add_persona, name='padd'),
    path('busca-tesis/', views.busca_tesis, name='busca-tesis'),
    path('api/', include('tesis.urls')),
    path('tabla-estudiante/subir-archivo/', views.subir_csv, name="subir-archivo"),
]
