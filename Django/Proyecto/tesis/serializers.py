from django.db.models import fields
from rest_framework import serializers
from tesis.models import *

class Estudiante_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Estudiante
        fields = '__all__'

class Tesis_Serializer(serializers.ModelSerializer):

    class Meta:
        model = Tesis
        fields = ['id','matricula','titulo','profesor','cotutor','informante','fecha','area']
        depth = 1


class Profesor_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Profesor
        fields = '__all__'

#<zg-column type="select" index="profesor" type-select-options="
 #             {% for i in profesores %}
  #            {{i.id}} {{ i.nombre}} {{i.apellido }},
   #           {% endfor %}" 
    #          ></zg-column>