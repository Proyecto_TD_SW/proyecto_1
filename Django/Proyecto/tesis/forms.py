from django import forms
from tesis.models import Estudiante, Tesis

class DateInput(forms.DateInput):
    input_type = 'date'

class EstudianteForm(forms.ModelForm):
    
    class Meta:
        model = Estudiante
        fields ="__all__"


class TesisForm(forms.ModelForm):
    cotutor = forms.CharField(label='Cotutor:',max_length=254)
    fecha = forms.DateField(widget=DateInput)
    class Meta:
        model = Tesis
        fields ="__all__"
        labels = {
            "matricula": " Estudiante "
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['matricula'].queryset = Estudiante.objects.filter(ocultar=False)