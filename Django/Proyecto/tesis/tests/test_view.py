from django.http import response
from django.test import TestCase
from tesis.models import *
from django.urls import reverse
# Create your tests here.


class inicioViewTest(TestCase):

    def test_view_url_exists_at_desired_location(self):
        print("\n_______Prueba de Inicio_______")
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
        print("La url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('inicio'))
        self.assertEqual(response.status_code, 200)
        print("La url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('inicio'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'inicio.html')
        print("La url de la vista esta utilizando la plantilla html correcta")

class academicosViewTest(TestCase):
    
    def test_view_url_exists_at_desired_location(self):
        print("\n_______Prueba de Academicos_______")
        response = self.client.get('/academicos/')
        self.assertEquals(response.status_code, 200)
        print("La url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('academicos'))
        self.assertEqual(response.status_code, 200)
        print("La url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('academicos'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'academicos.html')
        print("La url de la vista esta utilizando la plantilla html correcta")




class tabla_estudianteViewTest(TestCase):
    
    def test_view_url_exists_at_desired_location(self):
        print("\n_______Prueba de Tabla Estudiante_______")
        response = self.client.get('/tabla_estudiante')
        self.assertEquals(response.status_code, 301)
        print("La url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('tabla_estudiante'))
        self.assertEqual(response.status_code, 302)
        print("La url de la vista existe y es accesible correctamente por su nombre")





