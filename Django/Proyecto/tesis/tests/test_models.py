from django.test import TestCase
from tesis.models import *

# Create your tests here.
class estudianteModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n__________Test de Estudiante__________")
        # Set up non-modified objects used by all test methods
        Estudiante.objects.create(nombres='Nicolas', apellidos='Garrido', matricula='2014430839', rut='19.247.979-5', correo_institucional='nigarrido14@alumnos.utalca.cl', correo_alternativo='', telefono='', direccion='', ocultar=True)
        print("Test de ingresar datos a Estudiante se hizo de forma correcta")

    def test_nombre_label(self):
        estudiante_test = Estudiante.objects.get(matricula='2014430839')
        field_label = estudiante_test._meta.get_field('nombres').verbose_name
        self.assertEquals(field_label, 'nombres')
        print("Test label nombre de Estudiante se ejecutó correctamente")

    def test_estudiante_max_length(self):
        estudiante_max = Estudiante.objects.get(matricula='2014430839')
        max_length = estudiante_max._meta.get_field('nombres').max_length
        self.assertEquals(max_length, 40)
        print("Test max length de nombre de Estudiante se ejecutó correctamente")

    def test_estudiante_name_is_nombre(self):
        estudiante_name = Estudiante.objects.get(matricula='2014430839')
        expected_object_name = estudiante_name.nombres
        expected_object_name += ' '
        expected_object_name += estudiante_name.apellidos
        self.assertEquals(expected_object_name, str(estudiante_name))
        print("Test nombre de la Estudiante se ejecutó correctamente")

class profesorModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n__________Test de Profesor__________")
        # Set up non-modified objects used by all test methods
        Profesor.objects.create(nombre='Alejandro', apellido='Valdes', correo_profesor='avaldes@utalca.cl')
        print("Test de ingresar datos a Profesor se hizo de forma correcta")

    def test_nombre_label(self):
        estudiante_test = Profesor.objects.get(id='1')
        field_label = estudiante_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de Profesor se ejecutó correctamente")

    def test_estudiante_max_length(self):
        estudiante_max = Profesor.objects.get(id='1')
        max_length = estudiante_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 40)
        print("Test max length de nombre de Profesor se ejecutó correctamente")

    def test_estudiante_name_is_nombre(self):
        estudiante_name = Profesor.objects.get(id='1')
        expected_object_name = estudiante_name.nombre
        expected_object_name += ' '
        expected_object_name += estudiante_name.apellido
        self.assertEquals(expected_object_name, str(estudiante_name))
        print("Test nombre de la Profesor se ejecutó correctamente")

