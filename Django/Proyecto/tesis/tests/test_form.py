from django.test import TestCase
from tesis.forms import *
from tesis.models import *
from django.http import HttpRequest
from django.contrib.auth.models import User
# Create your tests here.

class estudianteFormTest(TestCase):

    def test_renew_form_nombre_field_label(self):
        form = EstudianteForm()
        self.assertFalse(form.fields['nombres'].label == "Nicolas")
        self.assertFalse(form.fields['apellidos'].label == "Garrido")
        self.assertFalse(form.fields['matricula'].label == "2014430839")
        self.assertFalse(form.fields['rut'].label == "19.247.979-7")
        self.assertFalse(form.fields['correo_institucional'].label == "nico14@alumnos.utalca.cl")
        self.assertFalse(form.fields['correo_alternativo'].label == "nico@gmail.com")
        self.assertFalse(form.fields['telefono'].label == "+56990205980")
        self.assertFalse(form.fields['direccion'].label == "7 sur 1220")
        self.assertFalse(form.fields['ocultar'].label == True)
        print("Test atributos en valiables del estudiante en el formulario se hizo de forma correcta")

    
    
    def test_renew_form_field_help_text(self):
        form = EstudianteForm()
        print("\n_________Test de Estudiante_________")
        self.assertEqual(form.fields['nombres'].help_text, '')
        self.assertEqual(form.fields['apellidos'].help_text, '')
        self.assertEqual(form.fields['matricula'].help_text, '')
        self.assertEqual(form.fields['rut'].help_text, '')
        self.assertEqual(form.fields['correo_institucional'].help_text, '')
        self.assertEqual(form.fields['correo_alternativo'].help_text, '')
        self.assertEqual(form.fields['telefono'].help_text, '')
        self.assertEqual(form.fields['direccion'].help_text, '')
        self.assertEqual(form.fields['ocultar'].help_text, '')
        print("Test help_text en valiables del estudiante en el formulario se hizo de forma correcta")


class TesisFormTest(TestCase):

    def test_renew_form_nombre_field_label(self):
        form = TesisForm()
        self.assertFalse(form.fields['matricula'].label == "1234567890")
        self.assertFalse(form.fields['titulo'].label == "Prueba Tesis")
        self.assertFalse(form.fields['profesor'].label == "1")
        self.assertFalse(form.fields['cotutor'].label == "Fabio Duran Verdugo")
        self.assertFalse(form.fields['informante'].label == "Pedro Valenzuela Jimenez")
        self.assertFalse(form.fields['fecha'].label == "2021-12-12")
        self.assertFalse(form.fields['area'].label == "Genética")
        print("Test atributos en valiables del estudiante en el formulario se hizo de forma correcta")

    
    
    def test_renew_form_field_help_text(self):
        form = TesisForm()
        print("\n_________Test de Estudiante_________")
        self.assertFalse(form.fields['matricula'].help_text, '')
        self.assertFalse(form.fields['titulo'].help_text, '')
        self.assertFalse(form.fields['profesor'].help_text, '')
        self.assertFalse(form.fields['cotutor'].help_text, '')
        self.assertFalse(form.fields['informante'].help_text, '')
        self.assertFalse(form.fields['fecha'].help_text, '')
        self.assertFalse(form.fields['area'].help_text, '')
        print("Test help_text en valiables del estudiante en el formulario se hizo de forma correcta")
