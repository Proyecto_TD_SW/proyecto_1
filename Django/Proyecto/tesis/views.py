import csv, io
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from django.conf import settings
from tesis.forms import EstudianteForm, TesisForm
from tesis.models import *
from django.urls import reverse
from django.http import Http404
from django.contrib.auth.decorators import login_required



def inicio(request):
    return render(request, "inicio.html")

def busca_tesis(request):
    return render(request, "busca_tesis.html")

def academicos(request):
    return render(request, "academicos.html")

def error(request):
    return render(request, "404.html")

@login_required
def inicio_login(request):
    return render(request, "inicio_login.html")

@login_required
def tabla_estudiante(request):
    return render(request, "tabla_estudiante.html")

@login_required
def profesores(request):
    return render(request, "profesor.html")

@login_required
def tesis(request):
    #profesores = Profesor.objects.all()
    form = TesisForm()
    return render(request, "tesis.html", {'settings': settings, 'form': form})

@login_required
def estudiantes(request):
    # obtenemos las Estudiante de la BD
    estudiantes = Estudiante.objects.all()
    # pasamos el resultado al template.
    return render(request, "tabla_estudiante.html", {'settings': settings, 'estudiantes': estudiantes})

@login_required
def subir_csv(request):    
    template = "subir-archivo.html"
    data = Estudiante.objects.all()
    if request.method == "GET":
        return render(request, template)    
    csv_file = request.FILES['file']    
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'No es un archivo CSV')    
    data_set = csv_file.read().decode('UTF-8')   
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=',', quotechar="|"):
        created = Estudiante.objects.update_or_create(
            nombres=column[0],
            apellidos=column[1],
            matricula=column[2],
            rut=column[3],
            correo_institucional=column[4],
            correo_alternativo=column[5],
            telefono=column[6],
            direccion=column[7],
            ocultar=column[8]
            
        )
    context = {}
    return render(request, template, context)


def add_persona(request):
    if request.method == 'POST': # si el usuario está enviando el formulario con datos
        form = TesisForm(request.POST) # Bound form
        print(form)
        if form.is_valid():
            
            nueva_tesis = form.save() # Guardar los datos en la base de datos

            return HttpResponseRedirect(reverse('tesis'))
    else:
        return HttpResponseRedirect(reverse('tesis'))

    return render(request, 'tesis.html', {'form': form})


