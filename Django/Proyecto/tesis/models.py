from django.db import models
from django.core.validators import RegexValidator
from django.urls import reverse
from django.utils import timezone

VALIDAR_LETRA = RegexValidator(r'^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$')
VALIDAR_MATRICULA = RegexValidator(r'^\d{10}$')
VALIDAR_NUMERO = RegexValidator(regex=r'^\+?1?\d{9,15}$')
VALIDAR_RUT = RegexValidator(r'^[0-9]+[0-9].[0-9]+[0-9]+[0-9].[0-9]+[0-9]+[0-9]-[0-9k]', message="Ingrese en formato: '11.111.111-1'")


class Estudiante(models.Model):
    nombres = models.CharField(max_length=40,validators=[VALIDAR_LETRA],blank=True)
    apellidos = models.CharField(max_length=40,validators=[VALIDAR_LETRA],blank=True)
    matricula = models.CharField(max_length=10,primary_key=True, validators=[VALIDAR_MATRICULA],blank=True)
    rut = models.CharField(max_length=12,validators=[VALIDAR_RUT],blank=True)
    correo_institucional = models.EmailField(blank=True)
    correo_alternativo = models.EmailField(blank=True)
    telefono = models.CharField(validators = [VALIDAR_NUMERO], max_length=12, blank=True)
    direccion = models.CharField(max_length=50, blank=True)
    ocultar = models.BooleanField(blank=True)

    def __str__(self):
        return self.nombres +' '+ self.apellidos


class Profesor(models.Model):
    nombre = models.CharField(max_length=40,validators=[VALIDAR_LETRA],blank=True)
    apellido = models.CharField(max_length=40,validators=[VALIDAR_LETRA],blank=True)
    correo_profesor = models.EmailField(blank=True)

    def __str__(self):
        return self.nombre +' '+ self.apellido


class Tesis(models.Model):
    day  = timezone.now()
    formatedDay  = day.strftime("%Y/%m/%d")


    id= models.AutoField(primary_key=True)
    matricula = models.ForeignKey(Estudiante,on_delete=models.CASCADE,null=True)
    titulo = models.CharField(max_length=40,validators=[VALIDAR_LETRA])
    profesor = models.ForeignKey(Profesor,on_delete=models.CASCADE,null=True)
    cotutor = models.CharField(max_length=40,validators=[VALIDAR_LETRA],blank=True)
    informante = models.CharField(max_length=40,validators=[VALIDAR_LETRA])
    fecha = models.DateField(default=formatedDay)
    select_area= (
        ('Informática','INFORMÁTICA'),
        ('Genómica','GENÓMICA'),
        ('Simulación','SIMULACIÓN'),
    )
    area = models.CharField(max_length=12,choices=select_area, default='Informática')

    def __str__(self):
        return str(self.matricula) +' '+ self.titulo

    def get_absolute_url(self):
        return reverse('tesis/', kwargs={'pk': self.pk, 'slug': self.slug })
