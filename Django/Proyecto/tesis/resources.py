from import_export import resources
from .models import *

class EstudianteResource(resources.ModelResource):
    class Meta:
        model = Estudiante
        exclude = ('id')
        import_id_fields = ['matricula']

