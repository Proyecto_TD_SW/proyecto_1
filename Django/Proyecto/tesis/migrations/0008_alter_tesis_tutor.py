# Generated by Django 3.2.9 on 2021-12-15 04:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tesis', '0007_auto_20211215_0430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tesis',
            name='tutor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tesis.profesor'),
        ),
    ]
