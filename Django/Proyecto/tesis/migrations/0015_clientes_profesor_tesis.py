# Generated by Django 3.2.9 on 2021-12-15 23:25

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tesis', '0014_auto_20211215_2324'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clientes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cliente', models.CharField(blank=True, max_length=40, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
                ('correo_cliente', models.EmailField(blank=True, max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Profesor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=40, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
                ('apellido', models.CharField(blank=True, max_length=40, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
                ('correo_profesor', models.EmailField(blank=True, max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Tesis',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matricula', models.CharField(blank=True, max_length=10, validators=[django.core.validators.RegexValidator('^\\d{10}$')])),
                ('titulo', models.CharField(blank=True, max_length=40, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
                ('tutor', models.CharField(blank=True, max_length=40, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
                ('cotutor', models.TextField(blank=True, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
                ('informante', models.CharField(blank=True, max_length=40, validators=[django.core.validators.RegexValidator('^[a-zA-Z\\s_]*$')])),
            ],
        ),
    ]
