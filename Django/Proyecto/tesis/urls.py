from django.urls import path,include
from tesis.api import *
from django.views.decorators.csrf import requires_csrf_token


urlpatterns = [
    path('estudiante/<int:pk>', editar_estudiante.as_view()),
    path('estudiante/',EstudenAPIView.as_view()),
    path('tesis/<int:pk>', editar_tesis.as_view()),
    path('tesis/',TesisAPIView.as_view()),
    path('profesor/<int:pk>', editar_profesor.as_view()),
    path('profesor/',ProfesorAPIView.as_view()),
]

