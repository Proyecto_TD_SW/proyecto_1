from django.contrib import admin
from django.db.models import fields

from .models import *
from import_export import resources 
from import_export.admin import ImportExportModelAdmin



class EstudianteAdmin(admin.ModelAdmin):
    #Listar datos de Estudiante
    list_display = ('nombres','apellidos','rut','matricula')

    #Eliminar boton DELETE
    def has_delete_permission(self, request, obj=None):
        return True
    
    #Metodo para dejar solo lectura edición 
    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('matricula','correo_institucional')
        return self.readonly_fields

# Registro de Estudiante.
admin.site.register(Estudiante,EstudianteAdmin)

# Registro de Profesor.
admin.site.register(Profesor)

# Registro de Profesor.
admin.site.register(Tesis)