# DESARROLLO DE SOFTWARE

En este repositorio se desarrolla el proyecto del curso "TALLER DESARROLLO DE SOFTWARE", 2021 semestre de primavera. Para las actualizaciones, busque [actualizaciones](https://gitlab.com/Proyecto_TD_SW/proyecto_1/-/commits/main/).

## Author

* [Nicolás Garrido Calderara](https://gitlab.com/Proyecto_TD_SW/proyecto_1)

## License

All the stuffs here are under MIT license. Read [the license](./LICENSE) for more information.
